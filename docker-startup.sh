/entrypoint.sh mongod --rest --fork --logpath /dev/null

# for each student in the students.txt file, execute the DB import script.
cat /startup/students.txt | xargs -n 1 ./startup/import-student-db.sh

while true; do sleep 1000; done