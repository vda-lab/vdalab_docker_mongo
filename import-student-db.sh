# bind STUDENT to the first argument
STUDENT=$1

# import the beers.json file as a collection in a db with name of the student
mongoimport --db $STUDENT --collection beers --drop --file /startup/beers.json